public class Factorial {


    public    int factorialOfNumber( int number){
         if(number==0){
             return 1;
         }
         return number * factorialOfNumber(number-1);
    }
}
